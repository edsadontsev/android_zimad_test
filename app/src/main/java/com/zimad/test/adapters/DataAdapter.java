package com.zimad.test.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.zimad.test.R;
import com.zimad.test.models.DataResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private Activity context;
    private List<DataResponse.Data> list;
    private int imageSize;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int title, String subTitle, String url);
    }

    public DataAdapter() {
        this.list = new ArrayList<>();
    }

    public List<DataResponse.Data> getList() {
        return list;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = (Activity) recyclerView.getContext();
        imageSize = context.getResources().getDimensionPixelSize(R.dimen.dimen50);
    }

    @Override
    public void onViewRecycled(DataAdapter.DataViewHolder holder) {
        Glide.get(context).clearMemory();
        super.onViewRecycled(holder);
    }

    @Override
    public DataAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data, parent, false);
        return new DataAdapter.DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataAdapter.DataViewHolder holder, final int position) {
        DataResponse.Data data = list.get(position);
        holder.title.setText(String.valueOf(position + 1));
        holder.subTitle.setText(data.title);
        loadPhoto(data.url,holder.imageView);

        holder.itemView.setOnClickListener(v->{
            onItemClickListener.onItemClick(position + 1, data.title, data.url);
        });
    }

    private void loadPhoto(String imageUrl, ImageView imageView) {
        imageView.measure(0,0);
        Glide.with(imageView.getContext().getApplicationContext())
                .asBitmap()
                .load(imageUrl)
                .apply(new RequestOptions()
                        .override(imageSize, imageSize)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .skipMemoryCache(true))
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image) ImageView imageView;
        @BindView(R.id.title) TextView title;
        @BindView(R.id.sub_title) TextView subTitle;

        DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}


