package com.zimad.test;

public interface Constants {

    String CAT = "cat";
    String DOG = "dog";

    String TITLE_EXTRA = "title_extra";
    String SUB_TITLE_EXTRA = "sub_title_extra";
    String IMAGE_URL_EXTRA = "image_url_extra";


}
