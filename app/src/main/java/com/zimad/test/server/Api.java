package com.zimad.test.server;

import com.zimad.test.models.DataResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.zimad.test.server.ApiService.BASE_URL;

public class Api {

    private Retrofit retrofit;
    private ApiService service;

    public static Api getInstance() {
        return new Api();
    }

    private Api() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(ApiService.class);
    }

    public Retrofit getRetrofitConnection() {
        return retrofit;
    }

    public Call<DataResponse> getData(String query) {
        return service.getData(query);
    }

}

