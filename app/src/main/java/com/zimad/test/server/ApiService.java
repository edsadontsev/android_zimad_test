package com.zimad.test.server;

import com.zimad.test.models.DataResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    String BASE_URL = "http://kot3.com/";

    @GET("xim/api.php")
    Call<DataResponse> getData(@Query("query") String query);
}
