package com.zimad.test;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zimad.test.adapters.DataAdapter;
import com.zimad.test.fragments.CatsFragment;
import com.zimad.test.fragments.DataFragment;
import com.zimad.test.fragments.DetailsFragment;
import com.zimad.test.fragments.DogsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, DataFragment.createInstance(), DataFragment.TAG);
        transaction.commit();
    }

    public void showDetailsFragment(String url, String title, String subTitle){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, DetailsFragment.createInstance(url,title,subTitle), DetailsFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
