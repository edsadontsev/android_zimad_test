package com.zimad.test.models;

import java.util.List;

public class DataResponse {

    public List<Data> data;

    public class Data{
        public String url;
        public String title;
    }
}

