package com.zimad.test.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.zimad.test.R;
import com.zimad.test.fragments.base.BaseFragment;

import butterknife.BindView;

public class DataFragment extends BaseFragment implements TabLayout.OnTabSelectedListener{

    public static final String TAG = DataFragment.class.getSimpleName();

    @BindView(R.id.tab_layout_main) TabLayout tabLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_data;
    }

    public static Fragment createInstance() {
        return new DataFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initTabs();
    }

    private void initTabs(){
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addOnTabSelectedListener(this);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) tab.setCustomView(getTabView(i));
            if(i==0) onTabSelected(tab);
        }
    }

    private View getTabView(int position) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.tabs_item, null);
        TextView title = view.findViewById(R.id.title);
        switch (position){
            case 0:
                title.setText("tab 1");
                break;
            case 1:
                title.setText("tab 2");
                break;
        }

        return view;
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                hideDogsFragment();
                showCatsFragment();
                break;
            case 1:
                hideCatsFragment();
                showDogsFragment();
                break;
        }
    }

    private void showCatsFragment(){
        Fragment catsFragment = getChildFragmentManager().findFragmentByTag(CatsFragment.TAG);
        if(catsFragment==null){
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().add(R.id.fragment_container,
                    CatsFragment.createInstance(),CatsFragment.TAG);
            transaction.commit();
        }
        else{
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().show(catsFragment);
            transaction.commit();
        }
    }

    private void showDogsFragment(){
        Fragment dogsFragment = getChildFragmentManager().findFragmentByTag(DogsFragment.TAG);
        if(dogsFragment==null){
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().add(R.id.fragment_container,
                    DogsFragment.createInstance(),DogsFragment.TAG);
            transaction.commit();
        }
        else{
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().show(dogsFragment);
            transaction.commit();
        }
    }

    private void hideCatsFragment(){
        Fragment catsFragment = getChildFragmentManager().findFragmentByTag(CatsFragment.TAG);
        if(catsFragment!=null){
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().hide(catsFragment);
            transaction.commit();
        }
    }

    private void hideDogsFragment(){
        Fragment dogsFragment = getChildFragmentManager().findFragmentByTag(DogsFragment.TAG);
        if(dogsFragment!=null){
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction().hide(dogsFragment);
            transaction.commit();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        TextView title = tab.getCustomView().findViewById(R.id.title);
        title.setTextColor(ContextCompat.getColor(getActivity(),android.R.color.holo_red_dark));
        setCurrentTabFragment(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        TextView title = tab.getCustomView().findViewById(R.id.title);
        title.setTextColor(ContextCompat.getColor(getActivity(),android.R.color.black));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
