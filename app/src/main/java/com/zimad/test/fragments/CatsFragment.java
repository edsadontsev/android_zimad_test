package com.zimad.test.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.zimad.test.MainActivity;
import com.zimad.test.R;
import com.zimad.test.adapters.DataAdapter;
import com.zimad.test.fragments.base.BaseFragment;
import com.zimad.test.models.DataResponse;
import com.zimad.test.server.Api;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zimad.test.Constants.CAT;

public class CatsFragment extends BaseFragment implements DataAdapter.OnItemClickListener{

    public static final String TAG = CatsFragment.class.getSimpleName();

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private Call<DataResponse> dataRequest;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_animal;
    }

    public static Fragment createInstance() {
        return new CatsFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getData();
    }

    @Override
    public void onDestroy() {
        if(dataRequest != null) dataRequest.cancel();
        super.onDestroy();
    }

    private void getData(){
        dataRequest = Api.getInstance().getData(CAT);
        dataRequest.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful()){
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    DataAdapter dataAdapter = new DataAdapter();
                    dataAdapter.setOnItemClickListener(CatsFragment.this);
                    dataAdapter.getList().addAll(response.body().data);
                    recyclerView.setAdapter(dataAdapter);
                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(int title, String subTitle, String url) {
        ((MainActivity)getActivity()).showDetailsFragment(url,String.valueOf(title),subTitle);
    }
}
