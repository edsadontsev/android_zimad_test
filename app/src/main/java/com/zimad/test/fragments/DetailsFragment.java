package com.zimad.test.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zimad.test.R;
import com.zimad.test.fragments.base.BaseFragment;

import butterknife.BindView;

import static com.zimad.test.Constants.IMAGE_URL_EXTRA;
import static com.zimad.test.Constants.SUB_TITLE_EXTRA;
import static com.zimad.test.Constants.TITLE_EXTRA;

public class DetailsFragment extends BaseFragment{

    public static final String TAG = DetailsFragment.class.getSimpleName();

    @BindView(R.id.image) ImageView imageView;
    @BindView(R.id.title) TextView titleTextView;
    @BindView(R.id.sub_title) TextView subTitleTextView;

    private String url;
    private String title;
    private String subTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_details;
    }

    public static Fragment createInstance(String url,String title, String subTitle) {
        DetailsFragment instance = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL_EXTRA, url);
        args.putString(TITLE_EXTRA, title);
        args.putString(SUB_TITLE_EXTRA, subTitle);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUI();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(IMAGE_URL_EXTRA);
            title = getArguments().getString(TITLE_EXTRA);
            subTitle = getArguments().getString(SUB_TITLE_EXTRA);
        }
    }

    private void initUI(){
        titleTextView.setText(title);
        subTitleTextView.setText(subTitle);
        Glide.with(getActivity())
                .load(url)
                .into(imageView);
    }
}
